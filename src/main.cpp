#include <functional>
#include <chrono>
#include <stdio.h>

template <typename RETURN>
class FlowStep{

    public: 
        FlowStep(std::function<RETURN()> $call) : call(std::move($call)){}
        RETURN run(void) { return call(); }
    private:
        std::function<RETURN()> const call; 
};

int square(int num); 

int square(int num){
    return num * num;
}

int main(void){
    
    FlowStep <void> fs1([]{printf("Hallo Welt!");});
    FlowStep <int> fs2([]{ return square(12); });
    FlowStep <double> fs3([]{ return (10.0 * 10.1); });
    
    fs1.run();
    printf( "%d\n",fs2.run());
    printf( "%f\n",fs3.run());
    
    return 0;
}