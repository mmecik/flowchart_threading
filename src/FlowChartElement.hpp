#include <functional>
#include <chrono>

template <typename RETURN>
class FlowChartElement{
    public:
        FlowChartElement(std::function<RETURN()> $call) : call(std::move($call)){}
        RETURN run(void) { call(); }
    private: 
        std::function<void()> const call; 
};